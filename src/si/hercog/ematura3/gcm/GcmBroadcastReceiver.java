package si.hercog.ematura3.gcm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		ComponentName mComponentName = new ComponentName(context,
				GcmIntentService.class.getName());
		Log.e("GcmBroadcastReceiver", "onReceive");
		startWakefulService(context, (intent.setComponent(mComponentName)));
		setResultCode(Activity.RESULT_OK);
	}
}
