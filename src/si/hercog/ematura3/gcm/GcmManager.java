package si.hercog.ematura3.gcm;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import si.hercog.ematura3.network.MySQLAccess;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmManager {

	private GoogleCloudMessaging mCloudMessaging;
	private Activity mActivity;
	private Context mContext;
	private SharedPreferences mSharedPreferences;

	private String mRegId = "";

	private final String SENDER_ID = "97883532316";

	private int mUserId = 0;

	int currentVersion = 0;
	int loggedVersion = 0;

	int timeout = 100;

	public GcmManager(Activity mActivity, SharedPreferences mSharedPreferences,
			int mUserId) {
		this.mActivity = mActivity;
		this.mContext = (Context) mActivity;
		this.mSharedPreferences = mSharedPreferences;
		this.mUserId = mUserId;

		if (mContext != null) {
			mCloudMessaging = GoogleCloudMessaging.getInstance(mContext);
			mRegId = getRegistrationdId();

			Log.i("GcmManager::Contructor", mRegId);

			if (mRegId.length() < 1) {
				obtainRegistrationKey();
			} else {
				Log.i("GcmManager::registrationKey", "Is set!");
			}

		} else {
			Log.i("GcmManager::Constructor", "mContext is null");
		}
	}

	public boolean checkGooglePlayService() {
		int mResultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(mContext);
		if (mResultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(mResultCode)) {
				GooglePlayServicesUtil.getErrorDialog(mResultCode, mActivity,
						mResultCode).show();
			} else {
				Log.i("GcmManager", "This device is not supported");
			}
			return false;
		}
		return true;
	}

	private String getRegistrationdId() {
		if (mSharedPreferences != null) {
			String gcm_id = mSharedPreferences.getString("gcm_id", "");

			if (gcm_id.length() > 0) {
				loggedVersion = mSharedPreferences.getInt(
						"application_version", Integer.MIN_VALUE);
				currentVersion = getApplicationVersion();

				if (currentVersion != loggedVersion) {
					Log.i("GCM Manager", "Application version changed");
					return "";
				}

				return gcm_id;
			} else {
				Log.i("GCM Manager", "Google Cloud Messaging ID is not set");
				return "";
			}

		}

		return "";
	}

	private int getApplicationVersion() {
		try {
			PackageInfo mPackageInfo = mContext.getPackageManager()
					.getPackageInfo(mContext.getPackageName(), 0);
			return mPackageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: "
					+ e.getMessage());
		}
	}

	private void obtainRegistrationKey() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					if (mCloudMessaging == null) {
						mCloudMessaging = GoogleCloudMessaging
								.getInstance(mContext);
					}

					mRegId = mCloudMessaging.register(SENDER_ID);
					registerGcmToServer();
					storeGcmToPreferences();

				} catch (IOException e) {
					Log.e("GCM Manager",
							"Problem with obtaining key... retrying");
					
					Looper.prepare();
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {

						@Override
						public void run() {
							obtainRegistrationKey();
						}
					}, timeout);

					timeout += 2 * timeout;
					e.printStackTrace();
				}

				return null;
			}

		}.execute(null, null, null);
	}

	private void registerGcmToServer() {
		new MySQLAccess((Activity) mContext).updateUserGcmId(mRegId, mUserId);
		Log.i("GcmManager", "mRegId=>" + mRegId);
	}

	private void storeGcmToPreferences() {
		if (mSharedPreferences != null) {
			SharedPreferences.Editor mEditor = mSharedPreferences.edit();
			mEditor.putInt("application_version", getApplicationVersion());
			mEditor.putString("gcm_id", mRegId);
			mEditor.commit();
		}
	}
}
