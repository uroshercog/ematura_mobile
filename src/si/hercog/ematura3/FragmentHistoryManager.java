package si.hercog.ematura3;

import java.util.ArrayList;

import android.util.Log;

public class FragmentHistoryManager {

	private ArrayList<FragmentName> mFragmentNamesHistory;

	enum FragmentName {
		BROWSE, NOBROWSE, CONTENT, CHAPTER, SUBCHAPTER, SUMMARY, LOCALSUMMARY
	}

	public FragmentHistoryManager() {
		mFragmentNamesHistory = new ArrayList<FragmentHistoryManager.FragmentName>();
	}

	public boolean addRecord(FragmentName mFragmentName) {
		if (mFragmentName != null) {
			for (FragmentName mName : mFragmentNamesHistory) {
				if (mFragmentName == mName) {
					Log.i("FHM=>addrecord()", "Entry already exists");
					return false;
				}
			}
			mFragmentNamesHistory.add(mFragmentName);
			Log.w("FHM=>addRecord()", mFragmentName.toString()
					+ " added to history");
			if (mFragmentNamesHistory.get(mFragmentNamesHistory.size() - 1) == mFragmentName) {
				return true;
			}
		}

		return false;
	}

	public boolean removeRecord() {
		Log.i("removeRecord1",getCurrentActiveName().toString());
		if (mFragmentNamesHistory.size() > 0) {
			switch (mFragmentNamesHistory.get(mFragmentNamesHistory.size() - 1)) {
				case BROWSE :
				default :
					return true;
				case CONTENT :
				case CHAPTER :
				case SUBCHAPTER :
				case SUMMARY :
				case LOCALSUMMARY :
					if (mFragmentNamesHistory.remove(mFragmentNamesHistory
							.size() - 1) != null) {
						Log.i("removeRecord2",getCurrentActiveName().toString());
						return false;
					} else {
						new NullPointerException("Cannot remove record");
					}
			}
		} else {
			throw new NullPointerException(
					"There are no records to be removed!");
		}

		return true;
	}

	public FragmentName getCurrentActiveName() {
		if (mFragmentNamesHistory.size() > 0) {			
			return mFragmentNamesHistory.get(mFragmentNamesHistory.size() - 1);
		} else {
			return null;
		}
	}

	public boolean clear() {
		return mFragmentNamesHistory.removeAll(mFragmentNamesHistory);
	}
}
