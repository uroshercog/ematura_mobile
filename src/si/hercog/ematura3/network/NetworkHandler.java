package si.hercog.ematura3.network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkHandler {

	private NetworkInfo ni;
	private ConnectivityManager cm;
	private EmaturaListener.NetworkStateListener mStateListener;

	public NetworkHandler(Context context,
			EmaturaListener.NetworkStateListener mStateL) {
		this.mStateListener = mStateL;
		cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (mStateListener != null) {

			IntentFilter mIntentFilter = new IntentFilter(
					ConnectivityManager.CONNECTIVITY_ACTION);

			BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					mStateListener.onNetworkStateChanged(isActive());
				}
			};

			context.registerReceiver(mBroadcastReceiver, mIntentFilter);
		}
	}

	private int getConnectivityStatus() {
		ni = cm.getActiveNetworkInfo();
		if (ni != null) {
			switch (ni.getType()) {
				case ConnectivityManager.TYPE_MOBILE :
				case ConnectivityManager.TYPE_MOBILE_DUN :
				case ConnectivityManager.TYPE_MOBILE_HIPRI :
				case ConnectivityManager.TYPE_MOBILE_MMS :
				case ConnectivityManager.TYPE_MOBILE_SUPL :
					return 2;
				case ConnectivityManager.TYPE_WIFI :
				case ConnectivityManager.TYPE_WIMAX :
					return 1;
				default :
					return 0;

			}
		} else {
			return -1;
		}
	}

	public boolean isActive() {
		switch (getConnectivityStatus()) {
			case 0 :
			case -1 :
			default :
				return false;
			case 1 :
			case 2 :
				return isServerReachable();
		}
	}

	public boolean isServerReachable() {
		ni = cm.getActiveNetworkInfo();
		if (ni != null && ni.isConnected()) {
			try {
				URL url = new URL("http://ematura.gcc.si");
				HttpURLConnection urlc = (HttpURLConnection) url
						.openConnection();
				urlc.setConnectTimeout(10 * 1000); // 10 s.
				urlc.connect();
				if (urlc.getResponseCode() == 200) {
					Log.i("Connection", "Success !");
					return true;
				} else {
					return false;
				}
			} catch (MalformedURLException e1) {
				return false;
			} catch (IOException e) {
				return false;
			}
		}
		return false;
	}
}
