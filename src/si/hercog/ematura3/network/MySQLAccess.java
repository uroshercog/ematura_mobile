package si.hercog.ematura3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.sqlite.SqlManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class MySQLAccess {

	private NetworkHandler mNetworkHandler;
	InputStream mInputStream = null;

	private Context mContext;

	public MySQLAccess(Context mContext) {
		this.mContext = mContext;
		mNetworkHandler = new NetworkHandler(mContext, null);
	}

	public User getUser(User user) throws JSONException {
		JSONArray mArray = getJSONArray("SELECT * FROM users WHERE username='"
				+ user.getUsername() + "'", "select");

		JSONObject mObject = (mArray != null) ? mArray.getJSONObject(0) : null;

		if (mObject == null)
			Log.i("MySQLAccess=>getJsonArray", "mObject is null");

		if (mObject != null) {
			Log.i("MySQLAccess=>getUser", "We have a real user WOOHOOO!");
			int mId = mObject.optInt("id");
			String mEmail = mObject.optString("email");
			String mRealname = mObject.optString("realname");
			int mRole = mObject.optInt("role");
			int mBan = mObject.optInt("ban");

			if (user.getPassword().equals(mObject.optString("password"))) {
				user.set(mId > 0 ? mId : -1, mEmail.length() > 0 ? mEmail : "",
						mRealname.length() > 0 ? mRealname : "",
						user.validateRole(mRole) ? mRole : 0,
						(mBan == 0 || mBan == 1) ? mBan : 1);

				manageFavourites(mObject.optString("fav"));

			}
		} else {
			user.set(0, "", "", 0, -2);
		}

		return user;
	}

	public void manageFavourites(String mJSONFavourites) throws JSONException {
		String TAG = "manageFavourites";

		JSONArray mOnlineArray = new JSONArray(mJSONFavourites);
		SqlManager mSqlManager = new SqlManager();
		mSqlManager.open(mContext);
		ArrayList<ListViewItem> mLocalFavourites = mSqlManager.getFavourites();
		ArrayList<ListViewItem> mUsedFavourites = new ArrayList<ListViewItem>();
		mSqlManager.close();

		if (mOnlineArray.length() < 1) {
			Log.i(TAG, "Uporabnik NIMA spletnih priljubljenih vsebin");
			mSqlManager.open(mContext);
			mSqlManager.removeAllFavourites();
			mSqlManager.close();
		} else {
			Log.i(TAG, "Uporabnik ima spletne priljubljene vsebine:"
					+ mOnlineArray.length());
			if (mLocalFavourites.size() > 0) {
				Log.i(TAG, "Uporabnik ima lokalne priljubljene vsebine:"
						+ mLocalFavourites.size());
				for (int i = 0; i < mOnlineArray.length(); i++) {
					Log.i(TAG, "Spletna vsebina �t:" + i);
					JSONObject mObject = mOnlineArray.getJSONObject(i);
					int mOnlineId = mObject.optInt("id");

					if (mLocalFavourites.size() == 0) {
						Log.i(TAG,
								"Ni lokalnih vsebin torej dodam vse ostale spletne");
						mSqlManager.open(mContext);
						mSqlManager.addFavourite(getSummary(mOnlineId));
						mSqlManager.close();
					}

					for (int k = 0; k < mLocalFavourites.size();) {
						Log.i(TAG, "Lokalna vsebina �t:" + k);
						if (mOnlineId == mLocalFavourites.get(k).getOnlineId()) {
							Timestamp mOnlineTimestamp = getOnlineFavouriteTimestamp(mOnlineId);
							Timestamp mLocalTimestamp = Timestamp
									.valueOf(mLocalFavourites.get(k)
											.getTimestamp());

							if (mOnlineTimestamp.after(mLocalTimestamp)) {
								Log.i(TAG,
										"Spletni povzetek je novej�i, lokalni bo posodobljen");
								mSqlManager.open(mContext);
								mSqlManager
										.updateFavourite(getSummary(mLocalFavourites
												.get(k).getOnlineId()));
								mSqlManager.close();
							} else {
								Log.i(TAG, "Spletni povzetek ni novej�i");
							}

							mUsedFavourites.add(mLocalFavourites.get(k));
							mLocalFavourites.remove(k);
							break;
						} else {
							if (k == (mLocalFavourites.size() - 1)) {
								Log.i(TAG,
										"Spletni povzetek ne obstaja lokalno, dodajam");
								mSqlManager.open(mContext);
								mSqlManager.addFavourite(getSummary(mOnlineId));
								mSqlManager.close();
							}
							break;
						}
					}

				}

				mSqlManager.open(mContext);
				Log.i(TAG,
						"Odstranjujem vse lokalne povzetke, ki jih ni na spletu");
				for (int i = 0; i < mLocalFavourites.size(); i++) {
					mSqlManager.removeFavourite(mLocalFavourites.get(i));
				}
				mSqlManager.close();

			} else {
				Log.i(TAG, "Uporabnik NIMA lokalnih priljubljenih vsebin");
				setupFavourites(mOnlineArray);
			}
		}

		/*EmaturaListener.GCMPagerUpdate mPager = (EmaturaListener.GCMPagerUpdate) mActivity;
		mPager.update();*/

	}

	private Timestamp getOnlineFavouriteTimestamp(int mId)
			throws IllegalArgumentException, JSONException {
		JSONArray mArray = getJSONArray(
				"SELECT time_s FROM table_povzetki WHERE id=" + mId, "select");
		return Timestamp.valueOf(mArray.getJSONObject(0).getString("time_s"));
	}

	private ListViewItem getSummary(int mId) throws JSONException {
		JSONArray mArray = getJSONArray(
				"SELECT * FROM table_povzetki WHERE id=" + mId, "select");
		JSONObject mObject = mArray.getJSONObject(0);
		ListViewItem mItem = new ListViewItem();

		mItem.setTitle(mObject.optString("naslov"))
				.setText(mObject.optString("tekst"))
				.setPredmet(mObject.optInt("predmet_id"))
				.setVsebina(mObject.optInt("vsebina_id"))
				.setPoglavje(mObject.optInt("poglavje_id"))
				.setPodpoglavje(mObject.optInt("podpoglavje_id"))
				.setPovzetek(mObject.optInt("povzetki_id"))
				.setTimestamp(mObject.optString("time_s"))
				.setOnlineId(mObject.optInt("id"));

		return mItem;
	}

	public ArrayList<ListViewItem> getSubjects() {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		JSONArray mItemsArray = getJSONArray(
				"SELECT * FROM table_predmeti ORDER BY zaporedje", "select");
		JSONObject mObject = null;

		if (mItemsArray != null && mItemsArray.length() > 0) {
			for (int i = 0; i < mItemsArray.length(); i++) {
				try {
					mObject = (mItemsArray != null) ? mItemsArray
							.getJSONObject(i) : null;
					if (mObject != null) {
						mItems.add(new ListViewItem()
								.setPredmet(mObject.optInt("predmet_id"))
								.setTitle(mObject.optString("naslov"))
								.setId(mObject.optInt("predmet_id"))
								.setIcon(mObject.optString("icon")));

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return mItems;
	}

	public ArrayList<ListViewItem> getContent(int predmet) {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		JSONArray mItemsArray = getJSONArray(
				"SELECT * FROM table_vsebine WHERE predmet_id=" + predmet
						+ " ORDER BY zaporedje", "select");

		JSONObject mObject = null;
		
		Log.i("hmmm....","neki bo");

		if (mItemsArray != null && mItemsArray.length() > 0) {
			for (int i = 0; i < mItemsArray.length(); i++) {
				try {
					mObject = (mItemsArray != null) ? mItemsArray
							.getJSONObject(i) : null;
					if (mObject != null) {
						mItems.add(new ListViewItem()
								.setPredmet(mObject.optInt("predmet_id"))
								.setVsebina(mObject.optInt("vsebina_id"))
								.setId(mObject.optInt("id"))
								.setTitle(mObject.optString("naslov"))
								.setOrder(mObject.optInt("zaporedje")));
						Log.i("hmmm....",mObject.optInt("predmet_id")+"|"+mObject.optInt("vsebina_id")+"|"+mObject.optInt("id")+"|"+mObject.optString("naslov")+"|"+mObject.optInt("zaporedje"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return mItems;
	}

	public ArrayList<ListViewItem> getChapters(int predmet, int vsebina) {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		JSONArray mItemsArray = getJSONArray(
				"SELECT * FROM table_poglavja WHERE predmet_id=" + predmet
						+ " AND vsebina_id= " + vsebina + " ORDER BY zaporedje",
				"select");

		JSONObject mObject = null;

		if (mItemsArray != null && mItemsArray.length() > 0) {
			for (int i = 0; i < mItemsArray.length(); i++) {
				try {
					mObject = (mItemsArray != null) ? mItemsArray
							.getJSONObject(i) : null;
					if (mObject != null) {
						mItems.add(new ListViewItem()
								.setPredmet(mObject.optInt("predmet_id"))
								.setVsebina(mObject.optInt("vsebina_id"))
								.setPoglavje(mObject.optInt("poglavje_id"))
								.setId(mObject.optInt("id"))
								.setTitle(mObject.optString("naslov"))
								.setOrder(mObject.optInt("zaporedje")));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return mItems;
	}

	public ArrayList<ListViewItem> getSubchapters(int predmet, int vsebina,
			int poglavje) {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		JSONArray mItemsArray = getJSONArray(
				"SELECT * FROM table_podpoglavja WHERE predmet_id=" + predmet
						+ " AND vsebina_id= " + vsebina + " AND poglavje_id= "
						+ poglavje + " ORDER BY zaporedje", "select");

		JSONObject mObject = null;

		if (mItemsArray != null && mItemsArray.length() > 0) {
			for (int i = 0; i < mItemsArray.length(); i++) {
				try {
					mObject = (mItemsArray != null) ? mItemsArray
							.getJSONObject(i) : null;
					if (mObject != null) {
						mItems.add(new ListViewItem()
								.setPredmet(mObject.optInt("predmet_id"))
								.setVsebina(mObject.optInt("vsebina_id"))
								.setPoglavje(mObject.optInt("poglavje_id"))
								.setPodpoglavje(
										mObject.optInt("podpoglavje_id"))
								.setId(mObject.optInt("id"))
								.setTitle(mObject.optString("naslov"))
								.setOrder(mObject.optInt("zaporedje")));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return mItems;
	}

	public boolean updateFavourites() {
		Log.e("MySQLAccess", "updating favourites");
		try {
			SharedPreferences mPreferences = mContext.getSharedPreferences(
					"user", 0);

			JSONArray mArray = getJSONArray("SELECT fav FROM users WHERE id="
					+ mPreferences.getInt("id", 0), "select");
			JSONObject mObject = mArray.getJSONObject(0);
			manageFavourites(mObject.optString("fav"));
			return true;

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return false;

	}

	private void setupFavourites(JSONArray mArray) {
		SqlManager mSqlManager = new SqlManager();
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();

		for (int i = 0; i < mArray.length(); i++) {
			try {
				JSONArray mItemArray = getJSONArray(
						"SELECT * FROM table_povzetki WHERE id="
								+ mArray.getJSONObject(i).optInt("id"),
						"select");
				JSONObject mObject = mItemArray.getJSONObject(0);

				ListViewItem mItem = new ListViewItem();
				mItem.setOnlineId(mObject.optInt("id"));
				mItem.setTitle(mObject.optString("naslov"));
				mItem.setText(mObject.optString("tekst"));
				mItem.setPredmet(mObject.optInt("predmet_id"));
				mItem.setVsebina(mObject.optInt("vsebina_id"));
				mItem.setPoglavje(mObject.optInt("poglavje_id"));
				mItem.setPodpoglavje(mObject.getInt("podpoglavje_id"));
				mItem.setPovzetek(mObject.getInt("povzetki_id"));
				mItem.setTimestamp(mObject.optString("time_s"));
				mItem.setIcon(getSubjectIcon(mObject.optInt("predmet_id")));

				mItems.add(mItem);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		mSqlManager.open(mContext);
		mSqlManager.addAllFavourites(mItems);
		mSqlManager.close();

	}

	public ArrayList<ListViewItem> getSummaries(int predmet, int vsebina,
			int poglavje, int podpoglavje) {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		JSONArray mItemsArray = getJSONArray(
				"SELECT * FROM table_povzetki WHERE predmet_id=" + predmet
						+ " AND vsebina_id= " + vsebina + " AND poglavje_id= "
						+ poglavje + " AND podpoglavje_id=" + podpoglavje
						+ " ORDER BY zaporedje", "select");

		JSONObject mObject = null;

		if (mItemsArray != null && mItemsArray.length() > 0) {
			for (int i = 0; i < mItemsArray.length(); i++) {
				try {
					mObject = (mItemsArray != null) ? mItemsArray
							.getJSONObject(i) : null;
					if (mObject != null) {
						mItems.add(new ListViewItem()
								.setPredmet(mObject.optInt("predmet_id"))
								.setVsebina(mObject.optInt("vsebina_id"))
								.setPoglavje(mObject.optInt("poglavje_id"))
								.setPodpoglavje(
										mObject.optInt("podpoglavje_id"))
								.setPovzetek(mObject.optInt("povzetki_id"))
								.setOnlineId(mObject.optInt("id"))
								.setTitle(mObject.optString("naslov"))
								.setOrder(mObject.optInt("zaporedje"))
								.setText(mObject.optString("tekst"))
								.setTimestamp(mObject.optString("time_s")));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return mItems;
	}

	public void updateUserGcmId(String mRegId, int mUserId) {
		getJSONArray("UPDATE users SET gcm_id='" + mRegId + "' WHERE id='"
				+ mUserId + "'", "update");
	}

	public void removeOnlineFavourite(int mId) throws JSONException {
		SharedPreferences mPreferences = mContext.getSharedPreferences("user",
				0);
		String jsonString = getJSONArray(
				"SELECT fav FROM users WHERE id="
						+ mPreferences.getInt("id", 0), "select").toString();
		jsonString = jsonString.replace("\\", "");
		jsonString = jsonString.substring(9, (jsonString.length() - 3));

		JSONArray mArray = new JSONArray(jsonString);
		JSONArray mNewArray = new JSONArray();

		for (int i = 0; i < mArray.length(); i++) {
			JSONObject mObject = mArray.getJSONObject(i);
			if (mId != mObject.getInt("id")) {
				mNewArray.put(mObject);
			}
		}

		getJSONArray("UPDATE users SET fav='" + mNewArray.toString()
				+ "' WHERE id=" + mPreferences.getInt("id", 0), "update");

	}

	public void addOnlineFavourite(int mId) throws JSONException {
		SharedPreferences mPreferences = mContext.getSharedPreferences("user",
				0);

		String jsonString = getJSONArray(
				"SELECT fav FROM users WHERE id="
						+ mPreferences.getInt("id", 0), "select").toString();
		jsonString = jsonString.replace("\\", "");
		jsonString = jsonString.substring(9, (jsonString.length() - 3));

		JSONArray mArray = new JSONArray(jsonString);
		mArray.put(new JSONObject().put("id", mId));

		getJSONArray("UPDATE users SET fav='" + mArray.toString()
				+ "' WHERE id=" + mPreferences.getInt("id", 0), "update");
	}

	public ArrayList<ListViewItem> search(ArrayList<ListViewItem> mItems,
			String query) {
		JSONArray mArray = getJSONArray(
				"SELECT * FROM table_povzetki WHERE MATCH(naslov,tekst) AGAINST ('"
						+ query + "' IN BOOLEAN MODE) LIMIT 0,10", "select");

		if (mArray != null && mArray.length() > 0) {
			JSONObject mObject = null;
			for (int i = 0; i < mArray.length(); i++) {
				try {
					mObject = mArray.getJSONObject(i);
					mItems.add(new ListViewItem()
							.setPredmet(mObject.optInt("predmet_id"))
							.setVsebina(mObject.optInt("vsebina_id"))
							.setPoglavje(mObject.optInt("poglavje_id"))
							.setPodpoglavje(mObject.optInt("podpoglavje_id"))
							.setPovzetek(mObject.optInt("povzetki_id"))
							.setOnlineId(mObject.optInt("id"))
							.setTitle(mObject.optString("naslov"))
							.setOrder(mObject.optInt("zaporedje"))
							.setText(mObject.optString("tekst"))
							.setTimestamp(mObject.optString("time_s"))
							.setIcon(
									getSubjectIcon(mObject.optInt("predmet_id"))));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		}

		return mItems;
	}

	private JSONArray getJSONArray(String query, String mType) {
		if (mNetworkHandler.isActive()) {
			Log.i("opacupa","more bit");
			ArrayList<NameValuePair> mNameValuePair = new ArrayList<NameValuePair>();
			final StringBuilder mStringBuilder = new StringBuilder();

			mNameValuePair.add(new BasicNameValuePair("ematura", "true"));
			mNameValuePair.add(new BasicNameValuePair("query", query));
			mNameValuePair.add(new BasicNameValuePair("type", mType));

			try {
				Log.i("klient","neki");
				final HttpClient httpClient = new DefaultHttpClient();
				final HttpPost httpPost = new HttpPost(
						"http://ematura.gcc.si/android/script.php");
				httpPost.setEntity(new UrlEncodedFormEntity(mNameValuePair));

				Thread thread = new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							Log.i("INPUTSREAM","STREAM");
							mInputStream = httpClient.execute(httpPost)
									.getEntity().getContent();
							BufferedReader bufferedReader = new BufferedReader(
									new InputStreamReader(mInputStream,
											"iso-8859-1"), 8);
							String line = null;

							while ((line = bufferedReader.readLine()) != null) {
								mStringBuilder.append(line + "\n");
								Log.i("adsdasdas",mStringBuilder.toString());
							}

							mInputStream.close();
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});

				thread.start();

				while (thread.isAlive()) {
					/*Prazen while da se ne vra�a prazna vrednost ?*/
				}

				Log.e("", mStringBuilder.toString());

				return (mStringBuilder.toString() != null && mStringBuilder
						.length() > 0) ? new JSONArray(
						mStringBuilder.toString()) : null;

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.i("MySQLAccess=>getJsonArray", "No connection :(");
		}

		return null;
	}

	public String getSubjectIcon(int mSubjectId) {
		if (mSubjectId > 0) {
			try {
				JSONArray mArray = getJSONArray(
						"SELECT icon FROM table_predmeti WHERE predmet_id="
								+ mSubjectId, "select");
				JSONObject mObject = mArray.getJSONObject(0);

				if (mObject != null) {
					Log.i("MySQLAccess","IKONA PREDMETA: " + mObject.optString("icon"));
					return mObject.optString("icon");					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
