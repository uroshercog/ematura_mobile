package si.hercog.ematura3.network;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;

import si.hercog.ematura3.R;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

public class User {
	private String username;
	private String password;
	private String email;
	private String realname;
	private String favourites;
	private int role;
	private int ban = -1;
	private int id = -1;

	public User set(int id, String email, String realname, int role, int ban) {
		this.id = id;
		this.email = email;
		this.realname = realname;
		this.role = role;
		this.ban = ban;
		return this;
	}

	public boolean login(Activity mActivity, String username, String password)
			throws JSONException {
		this.username = username;
		this.password = md5(password);
		SharedPreferences mSharedPreferences = mActivity.getSharedPreferences(
				"user", 0);
		SharedPreferences.Editor mEditor = mSharedPreferences.edit();
		if (validateUoP(username) && validateUoP(password)) {
			Log.i("User=>Validate", "Successful");
			if (new MySQLAccess(mActivity).getUser(this) != null) {
				if (getBan() == 0) {
					mEditor.clear();
					mEditor.putInt("id", id);
					mEditor.putString("username", username);
					mEditor.putString("email", getEmail());
					mEditor.putString("realname", getRealname());
					mEditor.putInt("role", getRole());
					mEditor.putBoolean("logged", true);
					mEditor.commit();					
					
					Toast.makeText(
							mActivity.getApplicationContext(),
							(mActivity.getResources().getString(
									R.string.welcome_toast)
									+ " " + getRealname()), Toast.LENGTH_SHORT)
							.show();
					return true;
				} else if (getBan() == 1) {
					Log.i("MySQLAccess=>login", "User banned");
					Toast.makeText(mActivity.getApplicationContext(),
							R.string.user_banned, Toast.LENGTH_SHORT).show();
				} else if (getBan() == -1) {
					Toast.makeText(mActivity.getApplicationContext(),
							R.string.wrong_user_or_pw, Toast.LENGTH_SHORT)
							.show();
				} else if (getBan() == -2) {
					Toast.makeText(mActivity.getApplicationContext(),
							R.string.no_connection, Toast.LENGTH_SHORT).show();
				}
			}
		}

		return false;
	}

	public boolean validateRole(int role) {
		switch (role) {
			case 0 :
			case 1 :
			case 9 :
				return true;
			default :
				return false;
		}
	}

	public boolean validateUoP(String valid) {
		if (valid.length() >= 5 && valid.length() <= 20) {
			String haystack = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ1234567890~!@#$%^&*()_+|-=\\{}[]:\";<>?,./";
			boolean isOk = false;

			for (int i = 0; i < valid.length(); i++) {
				if (haystack.contains(String.valueOf(valid.charAt(i)))) {
					isOk = true;
				} else {
					isOk = false;
					Log.i("User=>Validate",
							"Input is not valid => " + valid.charAt(i));
					break;
				}
			}
			return isOk;
		}

		return false;
	}

	private String md5(String password) {
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			byte[] hash = m.digest(password.getBytes("UTF-8"));
			StringBuilder sb = new StringBuilder(hash.length * 2);
			for (byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}

			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return null;
	}

	public int getBan() {
		return this.ban;
	}

	public String getEmail() {
		return this.email;
	}

	public String getRealname() {
		return this.realname;
	}

	public int getRole() {
		return this.role;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public int getId() {
		return this.id;
	}

	public String getFavourites() {
		return this.favourites;
	}

}
