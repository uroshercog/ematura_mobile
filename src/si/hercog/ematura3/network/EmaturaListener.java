package si.hercog.ematura3.network;

import java.util.ArrayList;

import si.hercog.ematura3.ListViewItem;

public interface EmaturaListener {
	interface Updatable {
		public void update();
	}

	interface NetworkStateListener {
		public void onNetworkStateChanged(boolean active);
	}

	interface SwitchFragment {
		public void switchToNextFragment(ArrayList<Integer> ints);
		public boolean switchToPreviousFragment();
		public void refreshFavourite();
		public void fromTab(EmaturaListener.LocalSummaryPager.TabName mName, ListViewItem mItem);
	}

	interface Favourite {
		public boolean handleFavourite(ListViewItem mItem);
	}

	interface FavouriteChanged {
		public void refresh();
	}
	
	interface LocalSummaryPager{
		public static enum TabName{
			BROWSE, SEARCH, FAVOURITES
		}
		
		public void switchToTab(TabName mName);
	}	
	
	interface GCMPagerUpdate{
		public void update();
	}
}
