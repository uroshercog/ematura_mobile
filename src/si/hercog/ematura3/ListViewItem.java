package si.hercog.ematura3;

public class ListViewItem {
	private int id = -1;
	private int order = -1;
	private String title = null;
	private String icon = null;
	private String text = null;
	private String timestamp = null;
	private int onlineId = -1;

	private int predmet = -1;
	private int vsebina = -1;
	private int poglavje = -1;
	private int podpoglavje = -1;
	private int povzetek = -1;

	public ListViewItem setPredmet(int predmet) {
		this.predmet = predmet;
		return this;
	}

	public ListViewItem setVsebina(int vsebina) {
		this.vsebina = vsebina;
		return this;
	}

	public ListViewItem setPoglavje(int poglavje) {
		this.poglavje = poglavje;
		return this;
	}

	public ListViewItem setPodpoglavje(int podpoglavje) {
		this.podpoglavje = podpoglavje;
		return this;
	}

	public ListViewItem setPovzetek(int povzetek) {
		this.povzetek = povzetek;
		return this;
	}

	public ListViewItem setId(int id) {
		this.id = id;
		return this;
	}

	public ListViewItem setTitle(String title) {
		this.title = title;
		return this;
	}

	public ListViewItem setOrder(int order) {
		this.order = order;
		return this;
	}

	public ListViewItem setIcon(String icon) {
		this.icon = icon;
		return this;
	}

	public ListViewItem setText(String text) {
		this.text = text;
		return this;
	}

	public ListViewItem setTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}
	
	public ListViewItem setOnlineId(int onlineId) {
		this.onlineId = onlineId;
		return this;
	}

	public int getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public int getOrder() {
		return this.order;
	}

	public String getIcon() {
		return this.icon;
	}

	public int getPredmet() {
		return this.predmet;
	}

	public int getVsebina() {
		return this.vsebina;
	}

	public int getPoglavje() {
		return this.poglavje;
	}

	public int getPodpoglavje() {
		return this.podpoglavje;
	}

	public int getPovzetek() {
		return this.povzetek;
	}

	public String getText() {
		return this.text;
	}

	public String getTimestamp() {
		return this.timestamp;
	}
	
	public int getOnlineId() {
		return this.onlineId;
	}

	public String toString() {
		return "ID: " + getId() + "|Zaporedje: " + getOrder() + "|Ikona: "
				+ getIcon() + "|Naslov: " + getTitle() + "|Tekst: " + getText()
				+ "|Predmet: " + getPredmet() + "|Vsebina: " + getVsebina()
				+ "|Poglavje: " + getPoglavje() + "|Podpoglavje: "
				+ getPodpoglavje() + "|Povzetek: " + getPovzetek()
				+ "|Timestamp: " + getTimestamp() + "|Online id: " + getOnlineId();
	}
}
