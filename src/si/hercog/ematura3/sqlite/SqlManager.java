package si.hercog.ematura3.sqlite;

import java.util.ArrayList;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.network.MySQLAccess;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SqlManager {

	private SqlHelper helper;
	private SQLiteDatabase database;
	private Context mContext;

	public SqlManager open(Context context) {
		helper = new SqlHelper(context, "ematura.db", null, 2);
		database = helper.getWritableDatabase();
		this.mContext = context;
		return this;
	}

	public void close() {
		helper.close();
	}

	public boolean isOpen() {
		return database.isOpen();
	}

	public ArrayList<ListViewItem> getFavourites() {
		ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
		Cursor cursor = database.rawQuery("SELECT * FROM table_povzetki", null);

		cursor.moveToFirst();

		for (int i = 0; i < cursor.getCount(); i++) {
			mItems.add(new ListViewItem()
					.setOnlineId(
							cursor.getInt(cursor.getColumnIndex("online_id")))
					.setTitle(cursor.getString(cursor.getColumnIndex("naslov")))
					.setText(cursor.getString(cursor.getColumnIndex("tekst")))
					.setPredmet(
							cursor.getInt(cursor.getColumnIndex("predmet_id")))
					.setVsebina(
							cursor.getInt(cursor.getColumnIndex("vsebina_id")))
					.setPoglavje(
							cursor.getInt(cursor.getColumnIndex("poglavje_id")))
					.setPodpoglavje(
							cursor.getInt(cursor
									.getColumnIndex("podpoglavje_id")))
					.setPovzetek(
							cursor.getInt(cursor.getColumnIndex("povzetek_id")))
					.setTimestamp(
							cursor.getString(cursor.getColumnIndex("time_s")))
					.setIcon(cursor.getString(cursor.getColumnIndex("icon"))));

			cursor.moveToNext();
		}

		cursor.close();

		return mItems;
	}

	public boolean removeFavourite(ListViewItem mItem) {

		return database.delete("table_povzetki",
				"online_id=" + mItem.getOnlineId(), null) > 0;

	}

	public boolean addFavourite(ListViewItem mItem) {
		if (mItem != null) {
			ContentValues cv = new ContentValues();
			cv.put("online_id", mItem.getOnlineId());
			cv.put("naslov", mItem.getTitle());
			cv.put("tekst", mItem.getText());
			cv.put("predmet_id", mItem.getPredmet());
			cv.put("vsebina_id", mItem.getVsebina());
			cv.put("poglavje_id", mItem.getPoglavje());
			cv.put("podpoglavje_id", mItem.getPodpoglavje());
			cv.put("povzetek_id", mItem.getPovzetek());
			cv.put("time_s", mItem.getTimestamp());
			cv.put("icon", new MySQLAccess(mContext).getSubjectIcon(mItem
					.getPredmet()));

			database.insert("table_povzetki", null, cv);
			return true;
		}
		return false;
	}

	public void addAllFavourites(ArrayList<ListViewItem> mItems) {
		for (ListViewItem mItem : mItems) {
			addFavourite(mItem);
		}
	}

	public boolean addRemark(int location, String text) {
		ContentValues cv = new ContentValues();
		cv.put("opomba", text);
		return database.update("table_povzetki", cv, "lokacija=" + location,
				null) > 0;
	}

	public boolean isFavourite(ListViewItem mItem) {

		Cursor cursor = database.rawQuery(
				"SELECT 1 FROM table_povzetki WHERE online_id=?",
				new String[]{Integer.toString(mItem.getOnlineId())});

		cursor.moveToFirst();
		boolean extist = cursor.getCount() > 0;
		cursor.close();

		return extist;
	}

	public void updateFavourite(ListViewItem mItem) {
		if (mItem != null) {
			ContentValues cv = new ContentValues();
			cv.put("naslov", mItem.getTitle());
			cv.put("tekst", mItem.getText());
			cv.put("predmet_id", mItem.getPredmet());
			cv.put("vsebina_id", mItem.getVsebina());
			cv.put("poglavje_id", mItem.getPoglavje());
			cv.put("podpoglavje_id", mItem.getPodpoglavje());
			cv.put("povzetek_id", mItem.getPovzetek());
			cv.put("time_s", mItem.getTimestamp());
			cv.put("icon", new MySQLAccess(mContext).getSubjectIcon(mItem
					.getPredmet()));

			database.update("table_povzetki", cv, "online_id=?",
					new String[]{Integer.toString(mItem.getOnlineId())});

			Log.i("updateFavourite", "POVZETEK JE POSODOBLJEN!");

		}
	}

	public boolean removeAllFavourites() {
		return database.delete("table_povzetki", null, null) > 0;
	}

	public ArrayList<ListViewItem> searchLocalDatabase(String mQuery,
			ArrayList<ListViewItem> mItems) {
		if (mQuery != null) {
			Cursor cursor = database
					.rawQuery(
							"SELECT * FROM table_povzetki WHERE naslov LIKE ? OR tekst LIKE ?",
							new String[]{mQuery, mQuery});
			cursor.moveToFirst();

			for (int i = 0; i < cursor.getCount(); i++) {
				mItems.add(new ListViewItem()
						.setOnlineId(
								cursor.getInt(cursor
										.getColumnIndex("online_id")))
						.setTitle(
								cursor.getString(cursor
										.getColumnIndex("naslov")))
						.setText(
								cursor.getString(cursor.getColumnIndex("tekst")))
						.setPredmet(
								cursor.getInt(cursor
										.getColumnIndex("predmet_id")))
						.setVsebina(
								cursor.getInt(cursor
										.getColumnIndex("vsebina_id")))
						.setPoglavje(
								cursor.getInt(cursor
										.getColumnIndex("poglavje_id")))
						.setPodpoglavje(
								cursor.getInt(cursor
										.getColumnIndex("podpoglavje_id")))
						.setPovzetek(
								cursor.getInt(cursor
										.getColumnIndex("povzetek_id")))
						.setTimestamp(
								cursor.getString(cursor
										.getColumnIndex("time_s")))
						.setIcon(
								cursor.getString(cursor.getColumnIndex("icon"))));

				cursor.moveToNext();
			}

			cursor.close();
		}
		return mItems;
	}
}
