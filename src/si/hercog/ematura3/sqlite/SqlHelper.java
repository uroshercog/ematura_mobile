package si.hercog.ematura3.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {
	
	/*
	 * TODO: FTS3
	 * */

	private final String QUERY_TABLE_POVZETKI = "CREATE TABLE table_povzetki (id INTEGER PRIMARY KEY, online_id INTEGER, predmet_id INTEGER, vsebina_id INTEGER, poglavje_id INTEGER, podpoglavje_id INTEGER, povzetek_id INTEGER, naslov VARCHAR, tekst TEXT, opomba VARCHAR, time_s TIMESTAMP, icon VARCHAR);";
	
	public SqlHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(QUERY_TABLE_POVZETKI);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
