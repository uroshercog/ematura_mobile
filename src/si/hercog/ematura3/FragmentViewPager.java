package si.hercog.ematura3;

import java.util.ArrayList;

import si.hercog.ematura3.FragmentHistoryManager.FragmentName;
import si.hercog.ematura3.fragment.FragmentBrowse;
import si.hercog.ematura3.fragment.FragmentBrowseNoCon;
import si.hercog.ematura3.fragment.FragmentChapter;
import si.hercog.ematura3.fragment.FragmentContent;
import si.hercog.ematura3.fragment.FragmentFavourite;
import si.hercog.ematura3.fragment.FragmentSearch;
import si.hercog.ematura3.fragment.FragmentSubchapter;
import si.hercog.ematura3.fragment.FragmentSummary;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.NetworkHandler;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class FragmentViewPager extends FragmentStatePagerAdapter
		implements
			EmaturaListener.SwitchFragment,
			EmaturaListener.NetworkStateListener {

	NetworkHandler mNetworkHandler;

	FragmentBrowse mBrowse;
	FragmentBrowseNoCon mBrowseNoCon;
	FragmentFavourite mFavourite;
	FragmentSearch mSearch;
	FragmentContent mContent;
	FragmentChapter mChapter;
	FragmentSubchapter mSubchapter;
	FragmentSummary mSummary;

	FragmentHistoryManager mFhm;
	FragmentManager mFm;

	Fragment mFragmentAtZero;
	Fragment mFragmentHolder;

	EmaturaListener.LocalSummaryPager mChangePager;

	EmaturaListener.LocalSummaryPager.TabName currentLocalFragment = null;

	public FragmentViewPager(FragmentManager mFragmentManager, Context context,
			EmaturaListener.LocalSummaryPager mChangePager) {
		super(mFragmentManager);

		this.mChangePager = mChangePager;

		mNetworkHandler = new NetworkHandler(context, this);

		mFm = mFragmentManager;
		mFhm = new FragmentHistoryManager();

		mBrowse = new FragmentBrowse();
		mBrowseNoCon = new FragmentBrowseNoCon();
		mSearch = new FragmentSearch();
		mFavourite = new FragmentFavourite();

		mContent = new FragmentContent();
		mChapter = new FragmentChapter();
		mSubchapter = new FragmentSubchapter();
		mSummary = new FragmentSummary(); 

		mBrowse.setListener(this);
		mContent.setListener(this);
		mChapter.setListener(this);
		mSubchapter.setListener(this);
		mSummary.setListener(this);
		mFavourite.setListener(this);
		mSearch.setListener(this);

		if (mNetworkHandler.isActive()) {
			mFragmentAtZero = mBrowse;
			mFhm.addRecord(FragmentName.BROWSE);
		} else {
			mFragmentAtZero = mBrowseNoCon;
			mFhm.addRecord(FragmentName.NOBROWSE);
		}		
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
			default :
			case 0 :
				return mFragmentAtZero;
			case 1 :
				return mFavourite;
			case 2 :
				return mSearch;
		}

	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public int getItemPosition(Object object) {
		if (object != mFragmentAtZero) {
			return POSITION_NONE;
		} else {
			return super.getItemPosition(object);
		}
	}

	@Override
	public void switchToNextFragment(ArrayList<Integer> ints) {
		if (mFragmentAtZero != null) {
			mFm.beginTransaction().remove(mFragmentAtZero).commit();
		}

		switch (mFhm.getCurrentActiveName()) {
			case BROWSE :
				if (mFhm.addRecord(FragmentName.CONTENT)) {
					mContent.setData(ints);
					mFragmentAtZero = mContent;
				}
				break;
			case CONTENT :
				if (mFhm.addRecord(FragmentName.CHAPTER)) {
					mChapter.setData(ints);
					mFragmentAtZero = mChapter;
				}
				break;
			case CHAPTER :
				if (mFhm.addRecord(FragmentName.SUBCHAPTER)) {
					mSubchapter.setData(ints);
					mFragmentAtZero = mSubchapter;
				}
				break;
			case SUBCHAPTER :
				if (mFhm.addRecord(FragmentName.SUMMARY)) {
					mSummary.setData(ints);
					mFragmentAtZero = mSummary;
				}
				break;
			case SUMMARY :
			default :
				break;

		}

		notifyDataSetChanged();

	}

	@Override
	public boolean switchToPreviousFragment() {
		if (mFhm.getCurrentActiveName() == FragmentName.BROWSE
				|| mFhm.getCurrentActiveName() == FragmentName.NOBROWSE) {
			return true;
		} else {
			if (mFragmentAtZero != null) {
				mFm.beginTransaction().remove(mFragmentAtZero).commit();
			}

			switch (mFhm.getCurrentActiveName()) {
				case CONTENT :
					mFragmentAtZero = mBrowse;
					mFhm.removeRecord();
					notifyDataSetChanged();
					break;
				case CHAPTER :
					mFragmentAtZero = mContent;
					mFhm.removeRecord();
					notifyDataSetChanged();
					break;
				case SUBCHAPTER :
					mFragmentAtZero = mChapter;
					mFhm.removeRecord();
					notifyDataSetChanged();
					break;
				case SUMMARY :
					mFragmentAtZero = mSubchapter;
					mFhm.removeRecord();
					notifyDataSetChanged();
					break;
				case LOCALSUMMARY :
					mFragmentAtZero = getPreviousFragment();
					notifyDataSetChanged();
					switch (currentLocalFragment) {
						default :
						case BROWSE :
							break;
						case FAVOURITES :
						case SEARCH :
							mChangePager.switchToTab(currentLocalFragment);
							currentLocalFragment = null;
							break;

					}
					break;
				default :
					break;

			}

			return false;
		}
	}

	public Fragment getPreviousFragment() {
		if (!mFhm.removeRecord()) {
			Log.i("getPreviousFragment", mFhm.getCurrentActiveName().toString());
			switch (mFhm.getCurrentActiveName()) {
				case LOCALSUMMARY :
				case BROWSE :
				default :
					return mBrowse;
				case CHAPTER :
					return mChapter;
				case CONTENT :
					return mContent;
				case NOBROWSE :
					return mBrowseNoCon;
				case SUBCHAPTER :
					return mSubchapter;
				case SUMMARY :
					return mSummary;
			}
		}

		return mBrowse;
	}

	public EmaturaListener.SwitchFragment getSwitch() {
		return this;
	}

	@Override
	public void onNetworkStateChanged(boolean active) {
		if (mFhm.clear()) {
			if (active) {
				if (mFhm.addRecord(FragmentName.BROWSE)) {
					mFragmentAtZero = mBrowse;
				}
			} else {
				if (mFhm.addRecord(FragmentName.NOBROWSE)) {
					mFragmentAtZero = mBrowseNoCon;
				}
			}

			notifyDataSetChanged();
		}
	}

	@Override
	public void refreshFavourite() {
		if (mFavourite != null) {
			mFavourite.refresh();
		}
	}

	@Override
	public void fromTab(EmaturaListener.LocalSummaryPager.TabName mName,
			ListViewItem mItem) {
		if (mFragmentAtZero != null) {
			mFm.beginTransaction().remove(mFragmentAtZero).commit();
		}

		if (mFhm.getCurrentActiveName() != FragmentName.LOCALSUMMARY) {
			mFhm.addRecord(FragmentName.LOCALSUMMARY);
		}

		FragmentSummary mLocalSummary = new FragmentSummary();
		mLocalSummary.setLocalData(mItem);
		mLocalSummary.setListener(this);
		mLocalSummary.setChangePagerListener(mChangePager);
		mFragmentAtZero = mLocalSummary;
		notifyDataSetChanged();
		mChangePager
				.switchToTab(EmaturaListener.LocalSummaryPager.TabName.BROWSE);
		currentLocalFragment = mName;

	}
}