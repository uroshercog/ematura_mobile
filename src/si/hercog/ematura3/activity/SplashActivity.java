package si.hercog.ematura3.activity;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import si.hercog.ematura3.BitmapManager;
import si.hercog.ematura3.R;
import si.hercog.ematura3.gcm.GcmManager;
import si.hercog.ematura3.network.User;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SplashActivity extends ActionBarActivity {

	private SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_splash);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		ActionBar mActionBar = getSupportActionBar();
		mActionBar.hide();

		pref = getApplication().getSharedPreferences("user", 0);

		if (pref.getInt("application_version", 0) < 1) {
			SharedPreferences.Editor mEditor = pref.edit();
			mEditor.putInt("application_version", getApplicationVersion());
			mEditor.commit();
		}

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				if (isLoggedIn()) {
					new Thread(new Runnable() {

						@Override
						public void run() {
							new BitmapManager(getApplicationContext());
						}
					}).start();

					new GcmManager(SplashActivity.this, pref, pref.getInt("id",
							0));
					startActivity(new Intent(getApplicationContext(),
							EmaturaActivity.class));
					finish();
				} else {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							login();
						}
					});
				}
			}
		}, 1500);
	}

	private int getApplicationVersion() {
		try {
			PackageInfo mPackageInfo = SplashActivity.this.getPackageManager()
					.getPackageInfo(SplashActivity.this.getPackageName(), 0);
			return mPackageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: "
					+ e.getMessage());
		}
	}

	private boolean isLoggedIn() {
		if (pref != null) {
			if (pref.getBoolean("logged", false)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private boolean login() {
		final ImageView logo = (ImageView) findViewById(R.id.splash_logo);
		final View loginView = (LinearLayout) findViewById(R.id.login_form);
		final EditText username = (EditText) loginView
				.findViewById(R.id.username);
		final EditText password = (EditText) loginView
				.findViewById(R.id.password);
		TranslateAnimation animationLogo = new TranslateAnimation(0, 0, 0, -100);
		final TranslateAnimation animationLogin = new TranslateAnimation(0, 0,
				0, 0);

		animationLogo.setDuration(300);
		animationLogo.setFillAfter(true);

		animationLogin.setDuration(300);
		animationLogin.setFillAfter(true);

		animationLogin.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				loginView.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				loginView.setVisibility(View.VISIBLE);
			}
		});

		logo.startAnimation(animationLogo);
		loginView.startAnimation(animationLogin);

		final Button btnLogin = (Button) loginView.findViewById(R.id.btnLogin);
		btnLogin.setEnabled(false);

		TextWatcher mTextWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if ((username.length() < 5 || username.length() > 20)
						|| (password.length() < 5 || password.length() > 20)) {
					btnLogin.setEnabled(false);
				} else {
					if (!btnLogin.isEnabled()) {
						btnLogin.setEnabled(true);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		};

		username.addTextChangedListener(mTextWatcher);
		password.addTextChangedListener(mTextWatcher);

		btnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Log.i("SplashActivity=>Login", username.getText()
							.toString() + "|" + password.getText().toString());

					if (new User().login(SplashActivity.this, username
							.getText().toString(), password.getText()
							.toString())) {
						new GcmManager(SplashActivity.this, pref, pref.getInt(
								"id", 0));
						startActivity(new Intent(getApplicationContext(),
								EmaturaActivity.class));
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});

		return true;
	}
}
