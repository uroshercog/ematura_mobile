package si.hercog.ematura3.activity;

import si.hercog.ematura.lib.TypefaceSpan;
import si.hercog.ematura3.FragmentViewPager;
import si.hercog.ematura3.FragmentViewPagerChanger;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import si.hercog.ematura3.network.NetworkHandler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;

public class EmaturaActivity extends ActionBarActivity
		implements
			EmaturaListener.LocalSummaryPager,
			EmaturaListener.GCMPagerUpdate {
	ViewPager mViewPager;
	NetworkHandler mNetworkHandler;
	ActionBar mActionBar;
	IntentFilter mIntentFilter;
	BroadcastReceiver mBroadcastReceiver;
	SpannableString mActionBarTitle;

	Tab mTabBrowse;
	Tab mTabFavourite;
	Tab mTabSearch;

	EmaturaListener.SwitchFragment mSwitch;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.ematura_activity_actions, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_refresh :
				if (new MySQLAccess(this).updateFavourites()) {
					mViewPager.getAdapter().notifyDataSetChanged();
				}
				break;
			case R.id.menu_settings :
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_ematura);

		mViewPager = (ViewPager) findViewById(R.id.ematura_pager);
		mActionBar = getSupportActionBar();
		FragmentViewPager mFragmentViewPager = new FragmentViewPager(
				getSupportFragmentManager(), EmaturaActivity.this, this);

		mTabBrowse = mActionBar.newTab();
		mTabBrowse.setText(R.string.browse);
		mTabBrowse.setTabListener(getTabListener());

		mTabFavourite = mActionBar.newTab();
		mTabFavourite.setText(R.string.favourite);
		mTabFavourite.setTabListener(getTabListener());

		mTabSearch = mActionBar.newTab();
		mTabSearch.setText(R.string.search);
		mTabSearch.setTabListener(getTabListener());

		mActionBarTitle = new SpannableString(getTitle());
		mActionBarTitle.setSpan(
				new TypefaceSpan(this, "GrandHotelRegular.otf"), 0,
				mActionBarTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		mActionBar.setTitle(mActionBarTitle);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		mActionBar.addTab(mTabBrowse);
		mActionBar.addTab(mTabFavourite);
		mActionBar.addTab(mTabSearch);

		mViewPager.setAdapter(mFragmentViewPager);
		mViewPager.setOnPageChangeListener(new FragmentViewPagerChanger(
				getSupportActionBar()));

		mSwitch = mFragmentViewPager.getSwitch();

	}

	private TabListener getTabListener() {
		TabListener mTabListener = new TabListener() {

			@Override
			public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
			}

			@Override
			public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
				mViewPager.setCurrentItem(arg0.getPosition());
			}

			@Override
			public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
			}
		};

		return mTabListener;
	}

	private void createReceiver() {
		mIntentFilter = new IntentFilter(
				ConnectivityManager.CONNECTIVITY_ACTION);
		mBroadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				updateFragments();
			}
		};

		registerReceiver(mBroadcastReceiver, mIntentFilter);

	}

	private void updateFragments() {
		switch (mActionBar.getSelectedTab().getPosition()) {
			case 0 :
				break;
			case 1 :
				break;
			case 2 :
				break;
			default :
				break;
		}
	}

	@Override
	protected void onPause() {
		unregisterReceiver(mBroadcastReceiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		createReceiver();
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		if (mActionBar.getSelectedNavigationIndex() == 0) {
			if (mSwitch.switchToPreviousFragment()) {
				super.onBackPressed();
			}
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void switchToTab(EmaturaListener.LocalSummaryPager.TabName mName) {
		switch (mName) {
			case BROWSE :
				mViewPager.setCurrentItem(0, true);
				mActionBar.setSelectedNavigationItem(0);
				break;
			case FAVOURITES :
				mViewPager.setCurrentItem(1, true);
				mActionBar.setSelectedNavigationItem(1);
				break;
			case SEARCH :
				mViewPager.setCurrentItem(2, true);
				mActionBar.setSelectedNavigationItem(2);
				break;
			default :
				break;
		}
	}

	@Override
	public void update() {
		if (mViewPager != null) {
			mViewPager.getAdapter().notifyDataSetChanged();
		}
	}
}