package si.hercog.ematura3;

import org.json.JSONException;

import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import si.hercog.ematura3.sqlite.SqlManager;
import android.content.Context;
import android.util.Log;

public class FavouriteManager implements EmaturaListener.Favourite {

	private SqlManager mSqlManager;
	private Context mContext;

	public FavouriteManager(Context mContext) {
		mSqlManager = new SqlManager();
		this.mContext = mContext;
	}

	@Override
	public boolean handleFavourite(ListViewItem mItem) {
		if (mItem != null) {
			if (!isFavourite(mItem)) {
				return addFavourite(mItem);
			} else {
				return removeFavourite(mItem);
			}
		}
		return false;
	}

	public boolean isFavourite(ListViewItem mItem) {
		mSqlManager.open(mContext);
		boolean favourite = mSqlManager.isFavourite(mItem);
		mSqlManager.close();

		return favourite;

	}

	public boolean addFavourite(ListViewItem mItem) {
		if (mItem != null) {
			mSqlManager.open(mContext);
			boolean added = mSqlManager.addFavourite(mItem);
			try {
				new MySQLAccess(mContext).addOnlineFavourite(mItem
						.getOnlineId());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			mSqlManager.close();
			if (added) {
				return added;
			}
		}
		return false;
	}

	public boolean removeFavourite(ListViewItem mItem) {
		if (mItem != null) {
			mSqlManager.open(mContext);
			boolean removed = mSqlManager.removeFavourite(mItem);

			try {
				Log.i("FavouriteManager", "" + mItem.getOnlineId());
				new MySQLAccess(mContext).removeOnlineFavourite(mItem
						.getOnlineId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mSqlManager.close();
			if (removed) {
				return false;
			}
		}
		return true;
	}
}