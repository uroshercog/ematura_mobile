package si.hercog.ematura3;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

public class FragmentViewPagerChanger implements ViewPager.OnPageChangeListener {

	ActionBar mActionBar;

	public FragmentViewPagerChanger(ActionBar mActionBar) {
		this.mActionBar = mActionBar;
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		mActionBar.setSelectedNavigationItem(arg0);
	}
}