package si.hercog.ematura3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BitmapManager {

	private URL mUrl;
	private HttpGet mHttpGet;
	private HttpClient mHttpClient;
	private HttpResponse mHttpResponse;
	private HttpEntity mHttpEntity;
	private BufferedHttpEntity mBufferedHttpEntity;
	private InputStream mInputStream;

	private Context mContext;

	public static ArrayList<String> cachedBitmaps;

	private final String TAG = "BitmapManager";

	public static boolean populated = false;

	public BitmapManager(Context mContext) {
		this.mContext = mContext;
		cachedBitmaps = new ArrayList<String>();
		Log.i(TAG, "---------------------------------");
		Log.i(TAG, "Started");
		Log.i(TAG, "Populating list of cached bitmaps");
		if (!populated) {
			populateCachedBitmapList();
			for (String mName : cachedBitmaps) {
				Log.i("CachedBitmapsName", mName);
			}
			populated = true;
		}
	}

	public Bitmap getBitmap(String mBitmapName) throws InterruptedException,
			ExecutionException {
		if (mBitmapName != null) {
			if (isCached(mBitmapName)) {
				Log.i(TAG, "Bitmap cached: " + mBitmapName);
				return getCachedBitmap(mBitmapName);
			} else {
				Log.i(TAG, "Bitmap NOT cached");
				return cacheBitmap(mBitmapName);
			}
		}

		return null;
	}

	private boolean isCached(String mBitmapName) {
		if (cachedBitmaps != null) {
			return cachedBitmaps.contains(mBitmapName);
		}
		return false;
	}

	private Bitmap getCachedBitmap(String mBitmapName) {
		File mFile;

		if (getExternalCacheDir() != null) {
			if ((mFile = getExternalCacheDir().getAbsoluteFile()) != null) {
				return BitmapFactory.decodeFile(mFile.getAbsolutePath() + "/"
						+ mBitmapName);
			} else {
				mFile = getInternalCacheDirectory().getAbsoluteFile();
				if (mFile != null) {
					return BitmapFactory.decodeFile(mFile.getAbsolutePath()
							+ "/" + mBitmapName);
				}
			}
		} else {
			mFile = getInternalCacheDirectory().getAbsoluteFile();
			if (mFile != null) {
				return BitmapFactory.decodeFile(mFile.getAbsolutePath() + "/"
						+ mBitmapName);
			}
		}

		return null;
	}
	private Bitmap cacheBitmap(final String mBitmapName)
			throws InterruptedException, ExecutionException {
		if (mBitmapName != null) {

			ExecutorService mExecutorService;
			Future<Bitmap> mTask;

			class CachedBitmapCallable implements Callable<Bitmap> {

				@Override
				public Bitmap call() throws Exception {
					Log.i(TAG, "CachedBitmapCallable called");
					mUrl = new URL("http://ematura.gcc.si/android/images/"
							+ mBitmapName);
					Log.i(TAG, "URL: OK");
					mHttpGet = new HttpGet(mUrl.toURI());
					Log.i(TAG, "GET: OK");
					mHttpClient = new DefaultHttpClient();
					Log.i(TAG, "CLIENT: OK");
					mHttpResponse = mHttpClient.execute(mHttpGet);
					Log.i(TAG, "RESPONSE: OK");
					mHttpEntity = mHttpResponse.getEntity();
					Log.i(TAG, "ENTITY: OK");
					mBufferedHttpEntity = new BufferedHttpEntity(mHttpEntity);
					Log.i(TAG, "BUFFERED: OK");
					mInputStream = mBufferedHttpEntity.getContent();
					Log.i(TAG, "STREAM: OK");

					Bitmap mBitmap = BitmapFactory.decodeStream(mInputStream);
					File mFile = new File(getActiveDirectory(), mBitmapName);
					OutputStream mOutputStream;

					if (mFile != null) {
						Log.i(TAG, "File path: " + mFile.getAbsolutePath());
						mOutputStream = new FileOutputStream(mFile);

						byte buffer[] = new byte[1024];

						mInputStream.reset();

						while (mInputStream.read(buffer) != -1) {
							mOutputStream.write(buffer);
							Log.i(TAG, "Writing file:" + buffer);
						}

						mInputStream.close();
						mOutputStream.close();

						cachedBitmaps.add(mBitmapName);

						Log.i(TAG, "FILE WRITING: OK");
						Log.i(TAG, "FILE SIZE: " + mFile.isFile());

					}

					return mBitmap;
				}
			}

			mExecutorService = Executors.newFixedThreadPool(1);
			mTask = mExecutorService.submit(new CachedBitmapCallable());

			Bitmap mBitmap = mTask.get();
			mExecutorService.shutdown();
			return mBitmap;

		}

		return null;
	}

	public void populateCachedBitmapList() {
		File[] mInternalFiles = getInternalFiles();
		File[] mExternalFiles = getExternalFiles();

		if (getExternalCacheDir() != null) {
			Log.i(TAG, "Looks like external is not null? huh");
			if (mInternalFiles.length > 0) {
				if (mExternalFiles.length > 0) {
					for (File mInternalFile : mInternalFiles) {
						for (File mExternalFile : mExternalFiles) {
							Log.i("hahsdjahsd", mInternalFile.getName() + "|"
									+ mExternalFile.getName());
							if (mInternalFile.getName().equals(
									mExternalFile.getName())) {
								if (mInternalFile.getAbsoluteFile()
										.lastModified() > mExternalFile
										.getAbsoluteFile().lastModified()) {
									moveToExternal(mInternalFile);
								} else {
									removeInternalFile(mInternalFile);
								}
							}
						}
					}
				} else {
					for (File mFile : mInternalFiles) {
						moveToExternal(mFile);
					}
				}
			}

			mExternalFiles = getExternalFiles();

			for (File mFile : mExternalFiles) {
				cachedBitmaps.add(mFile.getName());
			}

		} else {
			Log.i(TAG, "External is null!");
			Log.i(TAG, "Internal size: " + mInternalFiles.length);
			for (File mFile : mInternalFiles) {
				cachedBitmaps.add(mFile.getName());
			}
		}

	}
	private void moveToExternal(File mFileToMove) {
		if (mFileToMove != null) {
			if (getExternalCacheDir() != null) {
				Log.i(TAG, "Moving file from internal to external");
				try {
					File mMovedFile = new File(getExternalCacheDir()
							.getAbsolutePath() + "/" + mFileToMove.getName());

					InputStream mInputStream = new FileInputStream(mFileToMove);
					OutputStream mOutputStream = new FileOutputStream(
							mMovedFile);

					byte[] buffer = new byte[1024];

					while ((mInputStream.read(buffer)) != -1) {
						mOutputStream.write(buffer);
					}

					mInputStream.close();
					mOutputStream.close();

					Log.i(TAG, "File moved successfully");

					removeInternalFile(mFileToMove);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void removeInternalFile(File mFile) {
		if (mFile != null) {
			mFile.delete();
			Log.i(TAG, "File from internal removed");
		}
	}

	private File[] getExternalFiles() {
		File[] mExternalFiles = null;
		if (getExternalCacheDir() != null) {
			mExternalFiles = getExternalCacheDir().listFiles(
					new FilenameFilter() {

						@Override
						public boolean accept(File dir, String filename) {
							if (filename.lastIndexOf('.') > 0) {
								String extension = filename.substring(filename
										.lastIndexOf('.') + 1);
								if (extension.equals("png")) {
									return true;
								} else {
									return false;
								}
							} else {
								return false;
							}
						}
					});
		}

		return mExternalFiles;
	}

	private File[] getInternalFiles() {
		File[] mInternalFiles = getInternalCacheDirectory().listFiles(
				new FilenameFilter() {

					@Override
					public boolean accept(File dir, String filename) {
						if (filename.lastIndexOf('.') > 0) {
							String extension = filename.substring(filename
									.lastIndexOf('.') + 1);
							if (extension.equals("png")) {
								return true;
							} else {
								return false;
							}
						} else {
							return false;
						}
					}
				});
		return mInternalFiles;
	}

	public File getActiveDirectory() {
		Log.i(TAG, "Looking for active cache");

		if (getExternalCacheDir() != null) {
			return getExternalCacheDir();
		}

		return getInternalCacheDirectory();
	}

	private File getInternalCacheDirectory() {
		return mContext.getCacheDir();
	}

	private File getExternalCacheDir() {
		return mContext.getExternalCacheDir();
	}
}
