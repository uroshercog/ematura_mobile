package si.hercog.ematura3.fragment;

import java.util.ArrayList;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import si.hercog.ematura3.sqlite.SqlManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentSearch extends Fragment {

	private ArrayList<ListViewItem> mItems = new ArrayList<ListViewItem>();
	EmaturaListener.SwitchFragment mSwitch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_search, container, false);
		final EditText mSearchQuery = (EditText) v
				.findViewById(R.id.fragment_search_edittext_query);
		Button mSubmit = (Button) v
				.findViewById(R.id.fragment_search_button_submit);

		final RadioButton mLocal = (RadioButton) v
				.findViewById(R.id.fragment_search_radio_locally);
		final RadioButton mOnline = (RadioButton) v
				.findViewById(R.id.fragment_search_radio_online);

		ListView mListView = (ListView) v
				.findViewById(R.id.fragment_search_listview_matches);

		final Adapter mAdapter = new Adapter();

		mSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mItems.clear();
				if (mLocal.isChecked() && !mOnline.isChecked()) {
					SqlManager mSqlManager = new SqlManager();
					mSqlManager.open(getActivity());
					mItems = mSqlManager.searchLocalDatabase(mSearchQuery
							.getText().toString(), mItems);
					mSqlManager.close();
				} else if (!mLocal.isChecked() && mOnline.isChecked()) {
					mItems = new MySQLAccess(getActivity()).search(mItems,
							mSearchQuery.getText().toString());
				} else {
					Toast.makeText(getActivity(),
							"Prosim obkljukajte kje �elite iskati",
							Toast.LENGTH_SHORT).show();
				}

				if (mItems != null) {
					FragmentSearch.this.mItems = mItems;
					mAdapter.notifyDataSetChanged();
				}
			}
		});

		mListView.setAdapter(mAdapter);

		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mSwitch.fromTab(
						EmaturaListener.LocalSummaryPager.TabName.SEARCH,
						mItems.get(arg2));
			}
		});

		return v;
	}

	private class Adapter extends BaseAdapter {

		LayoutInflater mInflater;

		public Adapter() {
			mInflater = getActivity().getLayoutInflater();
		}

		@Override
		public int getCount() {
			return mItems.size();
		}

		@Override
		public Object getItem(int position) {
			return mItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return mItems.get(position).getOnlineId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(
						R.layout.fragment_favourite_listview_item, parent,
						false);
			}

			TextView tv = (TextView) convertView
					.findViewById(R.id.fragment_favourite_listview_item_textview);

			ImageView iv = (ImageView) convertView
					.findViewById(R.id.fragment_favourite_listview_item_icon);

			if (mItems != null) {
				tv.setText(mItems.get(position).getTitle());
				String formatName = mItems
						.get(position)
						.getIcon()
						.substring(0,
								mItems.get(position).getIcon().length() - 4)
						+ "_mini";

				int iconId = getResources().getIdentifier(formatName,
						"drawable", getActivity().getPackageName());

				iv.setImageResource(iconId);
			}

			return convertView;
		}

	}

	public FragmentSearch setListener(EmaturaListener.SwitchFragment mSwitch) {
		this.mSwitch = mSwitch;
		return this;
	}

}