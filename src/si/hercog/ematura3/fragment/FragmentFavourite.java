package si.hercog.ematura3.fragment;

import java.util.ArrayList;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.sqlite.SqlManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentFavourite extends Fragment
		implements
			EmaturaListener.FavouriteChanged {

	Adapter mAdapter;
	private ArrayList<ListViewItem> mItems;
	SqlManager mSqlManager;

	EmaturaListener.SwitchFragment mSwitch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater
				.inflate(R.layout.fragment_favourite, container, false);
		ListView listview = (ListView) v
				.findViewById(R.id.fragment_favourite_listview);

		mSqlManager = new SqlManager();
		mSqlManager.open(getActivity());
		mItems = mSqlManager.getFavourites();
		mSqlManager.close();

		mAdapter = new Adapter(getActivity(), mItems);

		listview.setAdapter(mAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mSwitch.fromTab(
						EmaturaListener.LocalSummaryPager.TabName.FAVOURITES,
						mItems.get(arg2));
			}
		});

		return v;
	}

	private class Adapter extends BaseAdapter {

		private ArrayList<ListViewItem> mItems;
		private LayoutInflater mInflater;

		public Adapter(Context mContext, ArrayList<ListViewItem> mItems) {
			this.mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.mItems = mItems;
		}

		@Override
		public int getCount() {
			if (mItems != null) {
				return mItems.size();
			}

			return 0;
		}

		@Override
		public Object getItem(int position) {
			if (mItems != null && mItems.size() > 0) {
				return mItems.get(position);
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(
						R.layout.fragment_favourite_listview_item, parent,
						false);
			}

			TextView tv = (TextView) convertView
					.findViewById(R.id.fragment_favourite_listview_item_textview);

			ImageView iv = (ImageView) convertView
					.findViewById(R.id.fragment_favourite_listview_item_icon);

			if (mItems != null) {
				tv.setText(mItems.get(position).getTitle());

				String formatName = mItems
						.get(position)
						.getIcon()
						.substring(0,
								mItems.get(position).getIcon().length() - 4)
						+ "_mini";

				int iconId = getResources().getIdentifier(formatName,
						"drawable", getActivity().getPackageName());				
				
				iv.setImageResource(iconId);
			}

			return convertView;
		}

		public void setItems(ArrayList<ListViewItem> mItems) {
			if (mItems != null) {
				this.mItems = mItems;
			}
		}

	}

	@Override
	public void refresh() {
		if (mAdapter != null && mSqlManager != null) {
			mSqlManager.open(getActivity());
			mItems = mSqlManager.getFavourites();
			mSqlManager.close();

			mAdapter.setItems(mItems);
			mAdapter.notifyDataSetChanged();
		}
	}

	public FragmentFavourite setListener(EmaturaListener.SwitchFragment mSwitch) {
		this.mSwitch = mSwitch;
		return this;
	}
}
