package si.hercog.ematura3.fragment;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import si.hercog.ematura3.BitmapManager;
import si.hercog.ematura3.FavouriteManager;
import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import si.hercog.ematura3.network.NetworkHandler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentSummary extends Fragment {

	private ArrayList<Integer> mData;

	private EmaturaListener.Favourite mFavourite;
	private EmaturaListener.SwitchFragment mSwitch;
	private EmaturaListener.LocalSummaryPager mChangePager;

	private FavouriteManager mFavouriteManager;

	private ListViewItem mItem;

	private static Pattern mPattern;

	@Override
	public void onAttach(Activity activity) {
		mFavouriteManager = new FavouriteManager(getActivity());
		mFavourite = (EmaturaListener.Favourite) mFavouriteManager;
		super.onAttach(activity);
	}

	private String getWebViewBaseUrl(String mSummaryText) {
		String t = "";

		for (int i = 0; i < mSummaryText.length(); i++) {
			if (mSummaryText.charAt(i) == '\'')
				t += '\\';
			if (mSummaryText.charAt(i) != '\n')
				t += mSummaryText.charAt(i);
			if (mSummaryText.charAt(i) == '\\')
				t += "\\";
		}

		t = "<script type='text/x-mathjax-config'>"
				+ "MathJax.Hub.Config({ "
				+ "showMathMenu: false, messageStyle: \"none\","
				+ "jax: ['input/TeX','output/HTML-CSS'], "
				+ "extensions: ['tex2jax.js'], "
				+ "TeX: { extensions: ['AMSmath.js','AMSsymbols.js',"
				+ "'noErrors.js','noUndefined.js'] } "
				+ "});</script>"
				+ "<script type='text/javascript' "
				+ "src='file:///android_asset/MathJax/MathJax.js'"
				+ "></script><span id='math'></span><script>javascript:document.getElementById('math').innerHTML='"
				+ t
				+ "';</script><script>javascript:MathJax.Hub.Queue(['Typeset',MathJax.Hub]);</script>";

		return t;
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ScrollView x = (ScrollView) inflater.inflate(R.layout.fragment_summary,
				container, false);
		LinearLayout v = (LinearLayout) x
				.findViewById(R.id.fragment_summary_linear);
		
		if (mItem != null) {
			RelativeLayout sItem = (RelativeLayout) inflater.inflate(
					R.layout.fragment_summary_item, null, false);
			TextView title = (TextView) sItem
					.findViewById(R.id.fragment_summary_item_title);
			LinearLayout ll = (LinearLayout) sItem
					.findViewById(R.id.fragment_summary_item_layout_text);
			WebView mWebView = new WebView(getActivity());

			final ImageView fav = (ImageView) sItem
					.findViewById(R.id.fragment_summary_add_fav);

			if (mFavouriteManager.isFavourite(mItem)) {
				fav.setImageResource(R.drawable.remove_fav);
			} else {
				fav.setImageResource(R.drawable.add_fav);
			}

			fav.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (new NetworkHandler(getActivity(), null).isActive()) {
						if (mFavourite.handleFavourite(mItem)) {
							fav.setImageResource(R.drawable.remove_fav);
							mSwitch.refreshFavourite();
						} else {
							fav.setImageResource(R.drawable.add_fav);
							mChangePager
									.switchToTab(EmaturaListener.LocalSummaryPager.TabName.FAVOURITES);
							mSwitch.switchToPreviousFragment();
							mSwitch.refreshFavourite();
						}

					} else {
						Toast.makeText(getActivity(),
								R.string.fav_no_con_disabled, Toast.LENGTH_LONG)
								.show();
					}
				}
			});

			title.setText(mItem.getTitle());

			mWebView.getSettings().setJavaScriptEnabled(true);
			mWebView.getSettings().setBuiltInZoomControls(false);
			mWebView.loadDataWithBaseURL("http://ematura.gcc.si",
					getWebViewBaseUrl(changeImageTagSource(mItem.getText()
							.toString())), "text/html", "UTF-8", "");

			ll.addView(mWebView);
			v.addView(sItem);
		} else {
			if (mData != null && v != null && mData.size() > 3) {
				final ArrayList<ListViewItem> mSummaries = new MySQLAccess(
						getActivity()).getSummaries(mData.get(0).intValue(),
						mData.get(1).intValue(), mData.get(2).intValue(), mData
								.get(3).intValue());
				if (mSummaries != null) {
					if (mSummaries.size() > 0) {
						for (int i = 0; i < mSummaries.size(); i++) {
							RelativeLayout sItem = (RelativeLayout) inflater
									.inflate(R.layout.fragment_summary_item,
											null, false);
							TextView title = (TextView) sItem
									.findViewById(R.id.fragment_summary_item_title);
							LinearLayout ll = (LinearLayout) sItem
									.findViewById(R.id.fragment_summary_item_layout_text);
							WebView mWebView = new WebView(getActivity());

							final ImageView fav = (ImageView) sItem
									.findViewById(R.id.fragment_summary_add_fav);

							if (mFavouriteManager
									.isFavourite(mSummaries.get(i))) {
								fav.setImageResource(R.drawable.remove_fav);
							} else {
								fav.setImageResource(R.drawable.add_fav);
							}

							final int y = i;

							fav.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (new NetworkHandler(getActivity(), null)
											.isActive()) {

										if (mFavourite
												.handleFavourite(mSummaries
														.get(y))) {
											fav.setImageResource(R.drawable.remove_fav);
										} else {
											fav.setImageResource(R.drawable.add_fav);
										}

										mSwitch.refreshFavourite();
									} else {
										Toast.makeText(getActivity(),
												R.string.fav_no_con_disabled,
												Toast.LENGTH_LONG).show();
									}
								}
							});

							title.setText(mSummaries.get(i).getTitle());

							mWebView.getSettings().setJavaScriptEnabled(true);
							mWebView.getSettings()
									.setBuiltInZoomControls(false);
							mWebView.loadDataWithBaseURL(
									"http://ematura.gcc.si",
									getWebViewBaseUrl(changeImageTagSource(mSummaries
											.get(i).getText().toString())),
									"text/html", "UTF-8", "");

							ll.addView(mWebView);
							v.addView(sItem);
						}
					} else {
						TextView tv = new TextView(getActivity());
						tv.setText("Ni vsebin za prikaz :(");
						v.addView(tv);
					}
				}
			}
		}

		return x;
	}
	public FragmentSummary setData(ArrayList<Integer> ints) {
		this.mData = ints;
		return this;
	}

	public FragmentSummary setListener(EmaturaListener.SwitchFragment mSwitch) {
		this.mSwitch = mSwitch;
		return this;
	}

	public FragmentSummary setChangePagerListener(
			EmaturaListener.LocalSummaryPager mChangePager) {
		this.mChangePager = mChangePager;
		return this;

	}

	public FragmentSummary setLocalData(ListViewItem mItem) {
		this.mItem = mItem;
		return this;
	}

	private String changeImageTagSource(String mText) {
		String mStringPattern = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";

		if (mPattern == null) {
			mPattern = Pattern.compile(mStringPattern);
		}
		Matcher mMatcher = mPattern.matcher(mText);

		BitmapManager mBitmapManager = new BitmapManager(getActivity());

		int i = 1;
		while (mMatcher.find()) {
			String name = mMatcher.group(i);
			String fileName = "";

			for (int j = name.length() - 1; j > 0; j--) {
				if (name.charAt(j) == '/') {
					break;
				} else {
					fileName = name.charAt(j) + fileName;
				}
			}

			i++;

			try {
				if (mBitmapManager.getBitmap(fileName) != null) {
					Log.i("dasdasdfsadsa", "ITS NOT NULL YUHEY!");
				}

				String resourceImage = "file://"
						+ mBitmapManager.getActiveDirectory().getAbsolutePath()
						+ "/" + fileName;

				if(mText.contains(name)) {
					mText = mText.replace(name, resourceImage);					
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}

		return mText;
	}
}