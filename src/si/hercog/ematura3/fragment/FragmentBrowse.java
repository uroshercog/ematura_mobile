package si.hercog.ematura3.fragment;

import java.util.ArrayList;
import java.util.Locale;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentBrowse extends Fragment {

	private View mView;

	private GridView mGridView;

	private ArrayList<ListViewItem> items;

	private EmaturaListener.SwitchFragment mSwitch;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater
				.inflate(R.layout.fragment_browse_con, container, false);
		if (items == null) {
			items = new MySQLAccess(getActivity()).getSubjects();
		}

		populateViewCon(inflater);

		return mView;
	}

	@SuppressLint("DefaultLocale")
	private void populateViewCon(final LayoutInflater inflater) {
		mGridView = (GridView) mView.findViewById(R.id.frag_browse_grid);

		mGridView.setAdapter(new BaseAdapter() {

			@Override
			public View getView(final int position, View convertView,
					ViewGroup parent) {
				if (convertView == null) {
					convertView = inflater.inflate(
							R.layout.fragment_browse_grid_item, parent, false);
				}

				TextView mItemTitle = (TextView) convertView
						.findViewById(R.id.fragment_browse_content_title);
				ImageView mItemIcon = (ImageView) convertView
						.findViewById(R.id.fragment_browse_content_icon);

				mItemTitle.setText(items.get(position).getTitle()
						.toUpperCase(Locale.getDefault()));

				int iconId = getResources().getIdentifier(
						items.get(position).getIcon(), "drawable",
						getActivity().getPackageName());
				mItemIcon.setImageResource(iconId);

				convertView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ArrayList<Integer> ints = new ArrayList<Integer>();
						Log.i("FragmentBrowse", ""
								+ items.get(position).getId());
						ints.add(items.get(position).getId());
						mSwitch.switchToNextFragment(ints);
					}
				});

				return convertView;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return items.get(position);
			}

			@Override
			public int getCount() {
				return items.size();
			}
		});

	}

	public FragmentBrowse setListener(EmaturaListener.SwitchFragment mSwitch) {
		this.mSwitch = mSwitch;
		return this;
	}
}
