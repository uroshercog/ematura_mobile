package si.hercog.ematura3.fragment;

import java.util.ArrayList;

import si.hercog.ematura3.ListViewItem;
import si.hercog.ematura3.R;
import si.hercog.ematura3.network.EmaturaListener;
import si.hercog.ematura3.network.MySQLAccess;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FragmentContent extends Fragment {

	private ArrayList<Integer> mData;
	private MySQLAccess mMySQLAccess;
	private String[] mStringArray;
	private ArrayList<ListViewItem> mItems;

	private EmaturaListener.SwitchFragment mSwitch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMySQLAccess = new MySQLAccess(getActivity());
		View v = inflater.inflate(R.layout.fragment_listview, container, false);

		Log.i("kliknem", "1");

		if (mData != null && v != null) {
			ListView listView = (ListView) v
					.findViewById(R.id.fragment_listview_main);

			Log.i("kliknem", "2");

			if ((mStringArray = getArray()) != null) {

				Log.i("kliknem", "3");

				if (listView != null) {
					listView.setAdapter(new ArrayAdapter<String>(getActivity(),
							R.layout.fragment_list_item, mStringArray));

					Log.i("kliknem", "4");

					if (mStringArray.length > 0) {
						if (mStringArray[0].equals("Ni poglavij")) {

						} else {
							listView.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int arg2, long arg3) {
									ArrayList<Integer> mDataInt = new ArrayList<Integer>();
									mDataInt.addAll(mData);
									mDataInt.add(mItems.get(arg2).getVsebina());

									mSwitch.switchToNextFragment(mDataInt);

									Log.i("kliknem", "5");
								}
							});
						}
					}
				}
			}
		}

		return v;
	}
	private String[] getArray() {
		String[] mStringArray = null;

		if (mData.size() == 1) {
			mItems = mMySQLAccess.getContent(mData.get(0));
			if (mItems != null && mItems.size() > 0) {
				mStringArray = new String[mItems.size()];
				for (int i = 0; i < mStringArray.length; i++) {
					mStringArray[i] = mItems.get(i).getOrder() + ". "
							+ mItems.get(i).getTitle();

					Log.i("asdasd", mStringArray[i] = mItems.get(i).getOrder()
							+ ". " + mItems.get(i).getTitle());
				}
			} else {
				mStringArray = new String[]{"Ni poglavij"};
			}
		}

		return mStringArray;
	}

	public FragmentContent setData(ArrayList<Integer> ints) {
		this.mData = ints;
		return this;
	}

	public FragmentContent setListener(EmaturaListener.SwitchFragment mSwitch) {
		this.mSwitch = mSwitch;
		return this;
	}
}
