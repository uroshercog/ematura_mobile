package si.hercog.ematura3.fragment;

import si.hercog.ematura3.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class FragmentTest extends Fragment {

	String example = "Povprecna hitrost v je definirana kot razmerje med premikom telesa in casom, v katerem je telo ta premik opravilo: <frm>\\(\\bar{v}=\\frac{\\Delta x}{\\Delta t}\\)</frm>";

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.test, container, false);
		WebView mWebView = (WebView) v
				.findViewById(R.id.fragment_summary_webview);

		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(false);

		mWebView.loadDataWithBaseURL(
				"http://bar",
				"<script type='text/x-mathjax-config'>"
						+ "MathJax.Hub.Config({ "
						+ "showMathMenu: false, "
						+ "jax: ['input/TeX','output/HTML-CSS'], "
						+ "extensions: ['tex2jax.js'], "
						+ "TeX: { extensions: ['AMSmath.js','AMSsymbols.js',"
						+ "'noErrors.js','noUndefined.js'] } "
						+ "});</script>"
						+ "<script type='text/javascript' "
						+ "src='file:///android_asset/MathJax/MathJax.js'"
						+ "></script><span id='math'></span><script>javascript:document.getElementById('math').innerHTML='"
						+ doubleEscapeTeX(example)
						+ "';</script><script>javascript:MathJax.Hub.Queue(['Typeset',MathJax.Hub]);</script>",
				"text/html", "utf-8", "");

		return v;
	}

	private String doubleEscapeTeX(String s) {
		String t = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\'')
				t += '\\';
			if (s.charAt(i) != '\n')
				t += s.charAt(i);
			if (s.charAt(i) == '\\')
				t += "\\";
		}
		return t;
	}
}
