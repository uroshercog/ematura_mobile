
### What is this repository for? ###

* eMatura is a content sharing system for students. It's main purpose is to provide quality content to students from teachers who input it in the system. The content consists of short summaries supported with pictures, graphs or mathematical formulas. Students can freely access the summaries either through the web application or the mobile application for android devices, they can also mark them as favourite. Doing so on a mobile device enables students browsing through the content even when offline.

### Who do I talk to? ###

* The author of the project is Uroš Hercog (uros@hercog.si)